---
layout: markdown_page
title: Total Rewards
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

Total Rewards at GitLab is made up of Benefits, Base Salary, Bonuses, Stock Options, and Perks.

## Benefits

[Benefits](/handbook/benefits/) at GitLab are determined based on our [Guiding Principles](/handbook/benefits/#guiding-principles). [General Benefits](/handbook/benefits/#general-benefits) (for example, [Paid time off](/handbook/paid-time-off/) policy) apply to all GitLab team members whereas [Entity Specific Benefits](/handbook/benefits/#entity-specific-benefits) apply based on the specific [contract type](/handbook/contracts/#employee-types-at-gitlab).

## Base Salary 

[Base Salary](/handbook/people-operations/global-compensation/) at GitLab is modeled using our [Compensation Principles](/handbook/people-operations/global-compensation/#compensation-principles) to [pay local rates](/handbook/people-operations/global-compensation/#paying-local-rates). 

All annual base pay only roles are seen within the [Compensation Calculator](/handbook/people-operations/global-compensation/calculator/).

## Bonuses 

* [Discretionary Bonus](/handbook/incentives/#discretionary-bonuses)
* [Referral Bonus](/handbook/incentives/#referral-bonuses)
* [Directors](/handbook/people-operations/global-compensation/#director-compensation), Executives, and certain Sales roles also have a variable bonus in addition to their base salary. 

## Stock Options

[Stock Options](/handbook/stock-options/) are available to all team members at GitLab and are not adjusted based on location.

## Perks 

* [Office Equipment and Supplies](/handbook/spending-company-money/)
* [Co-working Office Space]()
* [Work-related conferences]()
* [Year-end Holiday Party Budget]()
* [Visiting Grant](/handbook/incentives/#visiting-grant)
  * Grant to travel for fellow GitLabber's [Significant Life Event](https://about.gitlab.com/handbook/incentives/#significant-life-event-grants)
